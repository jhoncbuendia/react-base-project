class UserService {
    constructor() {
        console.log('ExampleService Class');
    }

    getUsers(callback) {
        console.log('getting fake data');
        fetch(
            'https://jsonplaceholder.typicode.com/users',
            {
                method:'GET'
            }
        )
        .then((response) => {
            return response.json();
        })
        .then((resource) => {
            callback(resource);
        })
    }
}

export default UserService;