class ExampleService {
    constructor() {
        console.log('ExampleService Class');
    }

    getFakeData(callback) {
        console.log('getting fake data');
        fetch(
            'https://jsonplaceholder.typicode.com/users',
            {
                method:'GET'
            }
        )
        .then((response) => {
            return response.json();
        })
        .then((resource) => {
            callback(resource);
        })
    }
}

export default ExampleService;