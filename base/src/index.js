import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import App from './App';
import Example from './components/example'
import Header from './components/header'
import UserList from './components/userList'

import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <div>
        <Header />
        <UserList />

    </div>
, document.getElementById('root'));
registerServiceWorker();
