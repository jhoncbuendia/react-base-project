import React, { Component } from 'react';

class ExampleChild extends Component {

    constructor(props) {
        super(props);
        this.state = {
            defaultMessage: this.props.defaultMessage
        }
    }

    changeDefaultMessage(message) {
        this.setState({defaultMessage:message})
    }

    componentWillMount() {
    }

    render() {
        console.log('2) render function');
        return (
        <div >
            <h1 >Component Example Child: {this.state.defaultMessage}</h1>
        </div>
        );
    }

    componentDidMount() {
    }

    componentWillReceiveProps(nextProps) {
        this.setState({defaultMessage:nextProps.defaultMessage})
    }
    
}

export default ExampleChild;