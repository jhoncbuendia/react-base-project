import React, { Component } from 'react';

class User extends Component {

    constructor(props) {
        super(props);
        this.state = {
            defaultMessage: this.props.defaultMessage,
            userInfo: this.props.userInfo
        }
        //console.log('ExampleChild Component');
    }

    changeDefaultMessage(message) {
        this.setState({defaultMessage:message})
    }

    componentWillMount() {
        //console.log('1) component will mount');
    }

    render() {
        //console.log('2) render function');
        return (
        <div>
            <p>Name: {this.state.userInfo.name} / Email: {this.state.userInfo.email}</p>
        </div>
        );
    }

    componentDidMount() {
        //console.log('3) component did mount');
    }

    componentWillReceiveProps(nextProps) {
        //console.log('4) componentWillReceiveProps');
        //console.log(nextProps);
    }
}

export default User;