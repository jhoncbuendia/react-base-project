import React, { Component } from 'react';
import UserService from '../services/user'
import User from './user';

class UserList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userList: []
        }

        this.userService = new UserService();
        //console.log('ExampleChild Component');
    }

    changeDefaultMessage(message) {
        this.setState({defaultMessage:message})
    }

    componentWillMount() {
        //console.log('1) component will mount');
        this.userService.getUsers( users=> {
            console.log(users);
            this.setState({userList:users})
        })
    }

    render() {
        //console.log('2) render function');
        return (
        <div>
            <h2>User List</h2>
            <ul>
                {
                    this.state.userList.map(user=>{
                        return (
                            <li>
                                <User userInfo={user}/>
                            </li>
                        )
                    })
                }
            </ul>
            User List
        </div>
        );
    }

    componentDidMount() {
        //console.log('3) component did mount');
    }

    componentWillReceiveProps(nextProps) {
        //console.log('4) componentWillReceiveProps');
        //console.log(nextProps);
    }
}

export default UserList;