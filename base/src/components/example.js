import React, { Component } from 'react';
import '../styles/example.css';
import ExampleChild from './exampleChild'
import ExampleService from '../services/example';
class Example extends Component {

    constructor(props) {
        super(props);
        this.state = {
            defaultMessage: this.props.defaultMessage
        }
        //console.log('ExampleChild Component');
    }

    changeDefaultMessage(message) {
        this.setState({defaultMessage:message})
    }

    componentWillMount() {
        //console.log('1) component will mount');
        let exampleService = new ExampleService();
        exampleService.getFakeData(resource => {
            console.log('resource');
            console.log(resource);
        } );
    }

    render() {
        //console.log('2) render function');
        let arr = [1,2,3,4];
        arr.map((element)=> {
            console.log(element);
        })

        return (
        <div className="main-container">
            <h1 >Component Example: {this.state.defaultMessage}</h1>
            <ul>
                {
                    arr.map((element)=> {
                        return(
                            <li value={element} 
                                onClick={
                                    (event)=> {
                                        console.log(event.target.value);
                                    }
                                }
                            >{element}</li>
                        );
                    })
                }
            </ul>
            <ExampleChild defaultMessage={this.state.defaultMessage}/>
            <input type='text' onChange={
                (event)=> {
                    console.log(event.target.value);
                    this.setState({defaultMessage:event.target.value})
                }
            }/>
            <button onClick={ ()=> {this.changeDefaultMessage('New Message') } }>Change state</button>
        </div>
        );
    }

    componentDidMount() {
        //console.log('3) component did mount');
    }

    componentWillReceiveProps(nextProps) {
        //console.log('4) componentWillReceiveProps');
        //console.log(nextProps);
    }
}

export default Example;